# Phony will be executed no matter what.
.PHONY: doc
.DEFAULT_GOAL := help
###########################################################################################################
## SCRIPTS
###########################################################################################################

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
        match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
        if match:
                target, help = match.groups()
                print("%-20s %s" % (target, help))
endef

###########################################################################################################
## VARIABLES
###########################################################################################################

export PRINT_HELP_PYSCRIPT
ifeq ($(OS),Windows_NT)
    export PYTHON=py
else
    export PYTHON=python3
endif


###########################################################################################################
## ADD TARGETS FOR YOUR TASK
###########################################################################################################

export PRINT_HELP_PYSCRIPT

help:
	@$(PYTHON) -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

doc: ## create documentation
	cd doc && $(MAKE) html
	@echo "open 'http://localhost:63342/python_scripts/doc/build/html/index.html' in a web browser"
	# not sure this link will work for all, maybe see doc_view

doc_view: ## host documentation
	cd doc/build/html && $(PYTHON) -m http.server

install:
	$(PYTHON) -m pip install .

demo:
	$(MAKE) demo_debugging
	$(MAKE) demo_listening

demo_debugging: ## Demonstrate Debugging
	$(PYTHON) -m python_scripts.debugging

demo_listening: ## Demonstrate Listener
	@echo "listening.py has not yet a demo"

clean:  ## delete every artifact
	cd doc && $(MAKE) clean