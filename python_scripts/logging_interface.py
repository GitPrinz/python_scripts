import os
import re

import logging
import datetime


def decrypt_path(input_path: str) -> str:

    findings_time = re.search('<time:(.*)>', input_path)
    if findings_time is not None:
        input_path = re.sub('<time:.*>', datetime.datetime.now().strftime(findings_time[1]), input_path)

    return input_path


class LoggerInterface:
    """ Not sure this class is really usefull. Only reason of existing is separation ... """

    _logger = logging.getLogger(__name__)  # will do nothing (i hope)

    def __init__(self, log_path: (None, str) = 'Logfiles/<time:%y%m%d_%H%M>.log'):

        if log_path is not None:

            if os.path.dirname(decrypt_path(log_path)) != '':
                os.makedirs(os.path.dirname(decrypt_path(log_path)), exist_ok=True)

            logging.basicConfig(filename=decrypt_path(log_path),
                                filemode='a',
                                format='%(asctime)s,%(msecs)03d %(name)s %(levelname)s %(message)s',
                                datefmt='%H:%M:%S',
                                level=logging.DEBUG)

            logging.addLevelName(25, 'STATUS')

            self._logger = logging.getLogger(__name__)

        return

    @staticmethod
    def end_logger() -> None:
        """ End logging and remove handle. """
        # todo: Do I use the logger wrong?
        #   The saved instance has no handlers ...
        logger = logging.getLogger()
        logger.handlers[0].stream.close()
        logger.removeHandler(logger.handlers[0])

    # Logging #
    ###########

    def debug(self, msg, *args, **kwargs):
        """ 10: Additional Information """
        self._logger.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """ 20: Information to execution of code """
        self._logger.info(msg, *args, **kwargs)

    def status(self, msg, *args, **kwargs):
        """ 25: Program starts or ends """
        self._logger.log(25, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """ 30: Warning """
        self._logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        """ 30: Error """
        self._logger.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        """ 40: Critical Error """
        self._logger.critical(msg, *args, **kwargs)

    def exception(self, msg, *args, **kwargs):
        """ 50: Unexpected Failure """
        self._logger.exception(msg, *args, **kwargs)
