#########
Debugging
#########

----
INFO
----
.. autoclass:: python_scripts.debugging.INFO
    :members:
    :undoc-members:
    :show-inheritance:

-----------
ProgramINFO
-----------
.. autoclass:: python_scripts.debugging.ProgramINFO
    :members:
    :undoc-members:
    :show-inheritance: