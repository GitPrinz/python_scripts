""" Collection of simple but maybe often used helpers """

from typing import Union


def str2num(value_str: str) -> Union[int, float]:
    """ Convert any number string to an integer or float

    <https://stackoverflow.com/questions/379906/how-do-i-parse-a-string-to-a-float-or-int>

    :param value_str: string like "123.45"
    :return: int (if possible) or float (will raise ValueError if not convertible)

    """
    assert isinstance(value_str, str), 'Expected a string! Got: '.format(type(value_str))
    try:
        return int(value_str)
    except ValueError:
        try:
            return float(value_str)
        except ValueError:
            raise ValueError('Unknown number: {}'.format(value_str))
