.. Python Scripts documentation master file, created by
   sphinx-quickstart on Fri Sep 11 16:08:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Scripts's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   debugging
   listening


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
