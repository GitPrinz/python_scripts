# Python Scripts for Logging, Debugging and more

Created for my needs.

Content:
- [debugging.py](python_scripts/debugging.py): Logging Routine
- [listening.py](python_scripts/listening.py): Event Handler
- [helpers.py](python_scripts/helpers.py): Minor support functions

Type in terminal:
- `make doc` and maybe `make doc_view` or visit the [Online Documentation](https://gitprinz.gitlab.io/python_scripts/)
- `make install` to install the package
- `make demo_debugging` to show debugging example

## ToDo

- [ ] Example for Listener
- [ ] Documentation for helpers

## History

### Version 0.3

0.3.3: Added helpers

0.3.2: Added Copy method

0.3.1: Now as package

### Version 0.2

0.2.2

- Adding "attachment_str" to info.start and ProgramINFO.

0.2.1

- Renaming of debugging/INFO parameters.  
    *But not the properties ... inconsequent?*
    - settings > info_settings
    - style > info_style
    - log_path > log_prefix/log_file
- Adding filename handling to automatically add data and time.
- Added Timing feature to keep track of duration.