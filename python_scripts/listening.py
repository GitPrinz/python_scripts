# Maybe checkout other Event Managers and decide for a common one
# <https://stackoverflow.com/questions/1092531/event-system-in-python>

from inspect import signature


class Listening:
    """ Add to class as parent or create as object to introduce Events.

    Attach functions with .add_listener(function) and trigger and event with .trigger_listeners()
    Number of expected parameters for listeners can be defined in initialization.
    """

    def __init__(self, num_parameters: int = 0):
        """

        :param num_parameters:

            defines how much arguments listeners have to take
            The wrong number of arguments will result in an error.
        """
        self._listeners = []
        self._num_parameters = num_parameters

    ###########
    # Methods #
    ###########

    def add_listener(self, listener: callable([[str], None])) -> None:
        """ Add new listener to list of listeners.

        :param listener: Some function which can take a message and will do things with it.
        """
        par = list(signature(listener).parameters)
        if len(par) != self._num_parameters:
            raise Exception('Expected {expected_num} Parameter for Listener. Got {is_num}!'.format(
                expected_num=self._num_parameters, is_num=len(par)))
        self._listeners.append(listener)
        return

    def trigger_listeners(self, *args, **kwargs) -> None:
        """ This function will call every attached listener. """
        for listener in self._listeners:
            listener(*args, **kwargs)
        return


# todo: make example
