import os
import datetime
from timeit import default_timer as timer

from typing import Union
from inspect import stack

from python_scripts.logging_interface import LoggerInterface


class INFO:
    """ Class for easy information handling (and for easy debugging)

    Suggestion for DebugLvL

        - 0 = Always On (Not Recommended)
        - 1 = Important Information
        - 2 = TopLevel Programs
        - 3 = Helpful Information
        - 4 = Relevant Programs
        - 5 = Further Information
        - 6 = Unimportant Programs
        - 7 = Unimportant Information
        - 8 = Subroutines
        - 9 = Everything
    """

    # Warning: The definition as class variables could lead to a connected behaviour of all INFO routines.
    _style = 'default'
    _stack = []
    _debug_lvl = 3
    _default_debug_value = 0
    _call_depth = 2
    _logger = None

    def __init__(self,
                 info_settings=None,
                 info_style: str = None, info_stack: list = None, debug_lvl: int = None,
                 log_prefix: Union[None, str] = None, log_file: Union[None, str] = None,
                 call_depth: int = None, suppress_ids: list = None):
        """ INFO Routine

        :param info_settings: Get settings from another info Routine.
        :param info_style: 'default' or 'fancy'.
        :param info_stack: Starts of blocks.
        :param debug_lvl: Overall DebugLvL to determine if calls are shown or not.
        :param log_prefix: Path to save logfile (will add ...).
        :param log_file: Filename to save logfile (only log_path or log_file).
        :param call_depth:
        :param suppress_ids: Message IDs which shouldn't show up.
        """

        self.restart_logger(log_prefix, log_file)

        if info_settings is not None:
            self.adapt_settings(info_settings)
        if info_stack is not None:
            assert isinstance(info_stack, list)
            self._stack = info_stack
        if debug_lvl is not None:
            self.debug_lvl = debug_lvl
        if info_style is not None:
            self.style = info_style
        if call_depth is not None:
            self.call_depth = call_depth

        if suppress_ids is None:
            self.suppress_ids = []
        else:
            self.suppress_ids = suppress_ids

        self.logger.debug('INFO initialized with:\n{}'.format({'info_style': self.style,
                                                               'info_stack': self.stack,
                                                               'debug_lvl': self.debug_lvl}))

        return

    ##############
    # Properties #
    ##############

    @property
    def logger(self):
        return self._logger

    @property
    def style(self):
        return self._style

    @style.setter
    def style(self, style: str):
        if style == 'default' or style[:5] == 'fancy':
            self._style = style
        elif style is None:
            self.style = 'default'
        else:
            raise Exception('"{}" is an unknown info_style'.format(self))
        return

    @property
    def stack(self) -> list:
        """ Starts of blocks. Only a copy, can not influence saved stack."""
        return [entry[0] if isinstance(entry, tuple) else entry for entry in self._stack]

    @property
    def debug_lvl(self):
        return self._debug_lvl

    @debug_lvl.setter
    def debug_lvl(self, value: int):
        assert isinstance(value, (int, float))
        self._debug_lvl = value
        return

    @property
    def default_debug_value(self):
        return self._default_debug_value

    @default_debug_value.setter
    def default_debug_value(self, value: int):
        assert isinstance(value, (int, float))
        self._default_debug_value = value
        return

    @property
    def call_depth(self):
        return self._call_depth

    @call_depth.setter
    def call_depth(self, value: int):
        assert isinstance(value, (int, float))
        self._call_depth = value
        return

    def get_space(self, offset: int = 0):
        return ' ' * (len(self.stack) * 2 - 1 + offset)

    ###########
    # Methods #
    ###########

    def restart_logger(self, log_prefix: Union[None, str] = None,
                       log_file: Union[None, str] = None) -> None:
        """ Create Logger

        This is untested and could lead to unexpected behavior.

        :param log_prefix: Path to save logfile (will add ...)
        :param log_file: Filename to save logfile (only log_path or log_file)
        """

        # prepare path
        if log_prefix is not None:
            if log_file is not None:
                raise Exception('Can only handle attribute log_prefix or log_file, not both!')

            # get start of filename
            log_file = os.path.basename(log_prefix)
            if len(log_file) == 0:
                log_file = 'Log'

            log_prefix = os.path.dirname(log_prefix)

            log_file = os.path.join(log_prefix, "{file_name}_{time}.log".format(
                file_name=log_file, time=datetime.datetime.now().strftime('%y%m%d_%H%M')))
        elif log_file is not None:
            log_prefix = os.path.dirname(log_file)

        if log_file is not None and len(log_prefix) > 0 and not os.path.exists(log_prefix):
            os.makedirs(log_prefix)

        # end logger
        if self._logger is not None:
            self.logger.status('Logging Ended (will continue at "{}")'.format(log_file))
            self.logger.end_logger()

        # start logger
        self._logger = LoggerInterface(log_file)
        self.logger.status('Logging Started')

    def adapt_settings(self, info_settings) -> None:
        """ Copy settings from another INFO routine

        :param info_settings: INFO or ProgramINFO class
        """

        if isinstance(info_settings, INFO):
            self._stack = info_settings._stack
            self.debug_lvl = info_settings.debug_lvl
            self.style = info_settings.style
        elif isinstance(info_settings, ProgramINFO):
            # this is untested
            self._stack = info_settings.info._stack
            self.debug_lvl = info_settings.info.debug_lvl
            self.style = info_settings.info.style
        else:
            raise Exception('Only Accept own instance to initialize. Got "{}"'.format(type(info_settings)))

        return

    def copy(self):
        """ Create unlinked copy of current settings. """
        new_info_obj = INFO()
        new_info_obj.adapt_settings(self)
        return new_info_obj

    def _console(self, info: str, indent_offset: int = 0):
        print(self.get_space(indent_offset) + info.replace('\n', '\n' + self.get_space(indent_offset)))

    def test_debug_value(self, _info: str, debug_value: int = None, *_args, **_kwargs) -> bool:
        if debug_value is None:
            debug_value = self.default_debug_value
        if debug_value <= self.debug_lvl:
            return True
        else:
            return False

    def fancy(self, debug_value: int = None) -> bool:
        if debug_value is None:
            debug_value = self.default_debug_value
        if self.style[:5] == 'fancy':
            if len(self.style) == 5:
                return True
            elif debug_value <= int(self.style[5:]):
                return True
            else:
                return False
        else:
            return False

    def happened_once(self, message_id: str) -> bool:
        if message_id in self.suppress_ids:
            return True
        else:
            self.suppress_ids.append(message_id)
            return False

    # Messages #
    ############

    def print(self, info: str, debug_value: int = None,
              indent_offset: int = 0, logging: bool = True, log_new_line: bool = False,
              once: str = None) -> bool:
        """ Default Output of Information.

        :param info: Content to print/log
        :param debug_value: Relevance of Message (see Class introduction)
        :param indent_offset: Modify indent manually
        :param logging: Extra Output in Logfile (default=True)
        :param log_new_line: Add new Line on Logfile (for formatted content over multiple lines, default=False)
        :param once: Code string will be saved. If already there, nothing will be printed.
        :return: False if nothing was printed (DebugSetting to low)
        """
        if once is not None and self.happened_once(once):
            return False
        if self.test_debug_value(info, debug_value):
            if logging:
                self.logger.info('\n{}'.format(info) if log_new_line else info)
            self._console(info, indent_offset)
            return True
        elif logging:
            self.logger.debug('\n{}'.format(info) if log_new_line else info)
        return False

    def print_add(self, info: str, debug_value: int = None,
                  indent_offset: int = 0, logging: bool = True) -> bool:
        """ Like print, but will have an linebreak in logfile and indent has +1"""
        return self.print(info, debug_value, indent_offset + 1, logging, log_new_line=True)

    def warn(self, info: str, _debug_value=None, indent_offset: int = 0, once: str = None, print_line: bool = True):
        """ Like print but will be always printed with source of warning.

        :param info: Content to print/log
        :param _debug_value: message is always printed and input will be ignored
        :param indent_offset: Modify indent manually
        :param once: Code string will be saved. If already there, nothing will be printed.
        :param print_line: if True (default) will add File and Line of call
        """
        if once is not None and self.happened_once(once):
            return
        self._console('WARNING: ' + info, indent_offset)
        self.logger.warning(info + ('\n' + self._get_file_and_line() if print_line else ''))

    def error(self, info: str, _debug_value=None, indent_offset: int = 0, print_line: bool = True) -> str:
        """ Like print but will be always printed with source of error.

        :param info: Content to print/log
        :param _debug_value: message is always printed and input will be ignored
        :param indent_offset: Modify indent manually
        :param print_line: if True (default) will add File and Line of call
        :return: info message to pass to exception

        :Note: This will not raise an error on its own.
        """
        self._console('ERROR: ' + info, indent_offset)
        self.logger.error(info + ('\n' + self._get_file_and_line() if print_line else ''))

        return info

    def headline(self, info: str, debug_value: int = None,
                 top_row: bool = None, bottom_row: bool = None, open_frame: bool = False,
                 indent_offset: int = 0, logging: bool = True) -> bool:
        """ Like print but can be styled with row of "###" before and/or after message.

        :param info: message string
        :param debug_value:
        :param top_row: show row before
        :param bottom_row: show row below
        :param open_frame:

            True will result in only rows before and below;
            False will close the box with leading and trailing # in the message line

        :param indent_offset:
        :param logging:
        :return:
        """

        if self.fancy(debug_value) and top_row is None and bottom_row is None:
            top_row = True
            bottom_row = True
        else:
            if top_row is None:
                top_row = False
            if bottom_row is None:
                bottom_row = False

        if self.test_debug_value(info, debug_value):
            if logging:
                self.logger.info('# ' + info)
            self._console(self.decorate(info, top_row=top_row, bottom_row=bottom_row, open_frame=open_frame),
                          indent_offset=indent_offset)
            return True
        elif logging:
            self.logger.debug('# ' + info)
        return False

    def start(self, name: str, debug_value: int = None, attachment_str: str = '',
              logging: bool = True, timing: bool = False) -> bool:
        """ Mark the start of a block. Returns True if it was printed. End block with ".end(...)".

        :param name: Name of block. E.g. Program name or what will be done.
        :param debug_value: Importance of block.
        :param attachment_str: Additional Information
        :param logging: Save in log (default=True, False)
        :param timing: Makes time_since_start() available if set to True.
        :return: True if printed.
        """
        # todo: empty call (collect filename)

        attachment_str = ' ({})'.format(attachment_str) if len(attachment_str) > 0 else ''

        if logging:
            if timing:
                self._stack.append((name, timer()))
            else:
                self._stack.append(name)
            self.logger.status('[RUN] ' + name + attachment_str)

        if self.test_debug_value(name, debug_value):
            if self.fancy(debug_value):
                self.headline(name, top_row=True, bottom_row=False, open_frame=True, indent_offset=-1)
            else:
                self.print('[RUN] ' + name + attachment_str, indent_offset=-1)
            return True
        else:
            return False

    def end(self, name: str, debug_value: int = None, logging: bool = True) -> bool:
        """ Closes previously started block. Returns True if

        Will print warning if nothing was opened or previously opened block is not the current block.
        Will print time duration if time is available.

        :param name: Name of block. E.g. Program name or what will be done.
        :param debug_value: Importance of block.
        :param logging: Save in log (default=True, False)
        """
        if logging:
            # stack empty
            if len(self.stack) < 1:
                self.logger.status('[END] ' + name + ' (Stack already empty)')
                print('Warning: Closing info_stack "{}" not possible, already empty.'.format(name))
                stack_time = None
                stack_time_str = ''

            # stack mismatch
            elif name != self.stack[-1]:
                self.logger.status('[END] ' + name + ' (mismatch to info_stack: "{}")'.format(self.stack[-1]))
                print('Warning: Should close "{get}", but last element is "{last}".'.format(get=name,
                                                                                            last=self.stack[-1]))
                stack_time = None
                stack_time_str = ''

            else:
                # regular (remove last stack)
                last_stack = self._stack.pop()
                stack_time = timer() - last_stack[1] if isinstance(last_stack, tuple) else None
                stack_time_str = '' if stack_time is None else ' (after {:.3f} Seconds)'.format(stack_time)
                self.logger.status('[END] ' + name + stack_time_str)
        else:
            stack_time = None
            stack_time_str = ''

        if self.test_debug_value(name, debug_value):
            if self.fancy(debug_value):
                self.headline(name, top_row=False, bottom_row=True, open_frame=True, indent_offset=+1)
                if stack_time is not None:
                    self.print('# After {:.0g} Seconds.'.format(stack_time), indent_offset=+1)
            else:
                self.print('[END] ' + name + stack_time_str, indent_offset=+1)
            return True

        else:
            return False

    def time_since_start(self) -> Union[float, None]:
        """
        :return: Time in seconds since call of ".start(..., timing=True, ...)" or None if not available.
        """
        return timer() - self._stack[-1][1]\
            if len(self._stack) > 0 and isinstance(self._stack[-1], tuple)\
            else None

    # intern Methods #
    @staticmethod
    def _get_file_and_line() -> str:
        stack_info = stack()
        for frame_info in stack_info:
            if not frame_info.filename.endswith('debugging.py'):
                return 'File "{file}", {line}'.format(file=frame_info.filename, line=frame_info.lineno)
        return 'unknown'

    # Statics #
    ###########

    @staticmethod
    def decorate(text: str, top_row: bool = True, bottom_row: bool = True, open_frame: bool = False) -> str:

        if top_row:
            top = '#' * (4 + len(text)) + '\n'
        else:
            if open_frame:
                top = '# {} #\n'.format(' ' * len(text))
            else:
                top = ''

        center = '# ' + text + ' #'

        if bottom_row:
            bottom = '\n' + '#' * (4 + len(text))
        else:
            if open_frame:
                bottom = '\n# {} #'.format(' ' * len(text))
            else:
                bottom = ''

        return '{top}{center}{bottom}'.format(top=top, center=center, bottom=bottom)

    # noinspection PyPep8Naming
    def ProgramINFO(self, program_name: str, debug_value: int = 0,
                    attachment_str: str = '', timing: bool = True):
        """ Return Class for Grouped Information with __enter__ and __exit__ statement (see PorgramINFO class) """
        return ProgramINFO(program_name=program_name, debug_value=debug_value,
                           attachment_str=attachment_str, info=self, timing=timing)


class ProgramINFO:

    buffer = None

    def __init__(self, program_name: str, debug_value: int = 0, info: INFO = INFO(),
                 attachment_str: str = '',
                 timing: bool = True, call_depth: int = None,
                 trigger_parent: callable([[int], None]) = None):
        """ Create an group of Info Output (started with [RUN] and ended with [END]).

        Replace info (INFO()) by: "with info.ProgramINFO('get_data_as_frame_extended', 6) as info:"
        to have more control over output.

        :param program_name: X in info.start(X) and info.end(X)
        :param debug_value: Y in info.start(X, Y) and info.end(X, Y)
        :param info: INFO object
        :param attachment_str: Additional information.
        :param timing: Will take track of time by default. Use False to prevent.
        :param call_depth: number of layers above which will be triggered if message comes through (np.inf) for infinity
        :param trigger_parent: function to trigger parent in info_stack
        """
        self.name = program_name
        self.attachment_str = attachment_str
        self.debug_value = debug_value
        self.info = info
        self.timing = timing
        if call_depth is None:
            self.call_depth = self.info.call_depth
        else:
            self.call_depth = call_depth
        self.trigger_parent = trigger_parent
        return

    def __enter__(self):
        if self.info.start(self.name, self.debug_value,
                           attachment_str=self.attachment_str, timing=self.timing):
            self.buffer = False
        else:
            self.buffer = True
        return self

    def __exit__(self, exc_type, exc_value, tb):

        if exc_type is not None:
            self.error('"{}" ended unexpected!'.format(self.name), indent_offset=1, print_line=False)
            self.info.end(self.name, self.debug_value)
            return False

        self.info.end(self.name, self.debug_value)

        return True

    # INFO Methods #
    ################

    def print(self, *args, **kwargs) -> bool:
        self._check_release(*args, **kwargs)
        return self.info.print(*args, **kwargs)

    def print_add(self, *args, **kwargs) -> bool:
        self._check_release(*args, **kwargs)
        return self.info.print_add(*args, **kwargs)

    def warn(self, *args, **kwargs):
        self._check_release(*args, **kwargs)
        self.info.warn(*args, **kwargs)

    def error(self, *args, **kwargs) -> str:
        self._check_release(*args, **kwargs)
        return self.info.error(*args, **kwargs)

    def headline(self, *args, **kwargs) -> bool:
        self._check_release(*args, **kwargs)
        return self.info.headline(*args, **kwargs)

    def start(self, *args, **kwargs) -> bool:
        self._check_release(*args, **kwargs)
        return self.info.start(*args, **kwargs)

    def end(self, *args, **kwargs) -> bool:
        self._check_release(*args, **kwargs)
        return self.info.end(*args, **kwargs)

    def copy(self):
        return self.info.copy()

    # Other Methods #
    def time_since_start(self) -> Union[float, None]:
        """
        :return: Time in seconds since start or None if not available.
        """
        return self.info.time_since_start()

    def _check_release(self, *args, **kwargs):
        if self.info.test_debug_value(*args, **kwargs) and self.buffer:
            if self.call_depth > 0 and self.trigger_parent is not None:
                self.trigger_parent(self.call_depth-1)
            self.info.start(self.name, self.info.debug_lvl, attachment_str=self.attachment_str, logging=False)
            self.buffer = False
        return

    def trigger(self, level: int = 0):
        """ Releases previously hidden messages."""
        if level > 0 and self.trigger_parent is not None:
            self.trigger_parent(level-1)
        # Prevent _check_release from triggering itself
        #   kind of Brute Force - is this possible nicer?
        call_depth = self.call_depth
        buffer = self.buffer
        self.call_depth = 0
        self._check_release('TEST CALL', self.info.debug_lvl)
        self.call_depth = call_depth
        self.buffer = buffer
        return

    # noinspection PyPep8Naming
    def ProgramINFO(self, *args, **kwargs):
        return ProgramINFO(*args, **kwargs, info=self.info, trigger_parent=self.trigger)


# Testing #
###########

if __name__ == '__main__':

    info0 = INFO(log_prefix='Test', debug_lvl=5, info_style='fancy3')

    info0.start('Layer 1', 2)
    info0.print('Visible Message 1', 3)
    info0.print('Visible Message 2', 3)
    info0.print_add('Extension to Visible Message 2', 3)

    # general Messages
    info0.warn('This is a warning message')
    info0.error('This is an error message')

    # Styled Messages
    info0.headline('Headline', 3)

    # Unstyled Messages
    info0.start('Layer 2 (timed)', 4, timing=True)
    print('>If active, you can access time since start whenever you want: {}'.format(info0.time_since_start()))
    info0.headline('Headline', 5)
    info0.end('Layer 2 (timed)', 4)  # manual layers have to be closed manually

    try:
        with info0.ProgramINFO('Layer 3', 4) as info2:
            info2.print('Normal Message', 5)
            print('>A call with ProgramINFO will be time by default: {}'.format(info2.time_since_start()))
            raise NotImplementedError(info2.error('This is an error message'))
    except NotImplementedError as err:
        info2.print('There was an error. The Group was closed anyway', 5)
        pass

    info0.start('Hidden Layer 1', 6)
    print('>Others have no time available by default: {}'.format(info0.time_since_start()))
    info0.print('Invisible Message 1', 7)
    info0.print('Visible Message 3', 3)

    with info0.ProgramINFO('Triggered Hidden Layer 1', 6) as info1:
        # usually this happens at the begin of a function
        info1.print('Invisible Message 2', 7)

        with info1.ProgramINFO('Triggered Hidden Layer 2', 6, attachment_str='Some more info!') as info2:

            info2.print('Invisible Message 3', 7)

            info2.start('Hidden Layer 2 (timed)', 8, timing=True)
            print('>Timing works for Hidden Layers too: {}'.format(info2.time_since_start()))
            info2.end('Hidden Layer 2 (timed)', 8)

            info2.start('Hidden Layer 3', 8)
            with info2.ProgramINFO('Triggered Hidden Layer 3', 8) as info3:
                info3.print('Invisible Message 3', 9)
                info3.print('Visible Message 4', 5)  # This triggers everything

            info2.end('Hidden Layer 3', 8)
            
            info2.print('Visible Message 5', 5)  # Triggers lower levels again

    # manual opened Layers (.start("...")) have to be closed manually
    info0.end('Hidden Layer 1', 6)
    info0.end('Layer 1', 2)

    info0.restart_logger(log_file='Test_new_path.log')
    info0.print('A Message in another Logfile.', 0)
